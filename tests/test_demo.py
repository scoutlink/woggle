"""
Woggle - bot framework

  Copyright (C) 2021 Christos Triantafyllidis <christos.triantafyllidis@gmail.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import pytest
from sopel.tests import rawlist
from sopel.tests.factories import BotFactory, ConfigFactory

TEST_CONFIG = """
[core]
owner = scoutlink
nick = woggle
enable = demo
host = test.scoutlink.net
extra = plugins

[permissions]
my_permission = user1

"""


@pytest.fixture(scope="module", name="bot")
def fixture_bot(tmpdir_factory):
    """
    A bot instance with demo module loaded
    """
    _tmp_dir = tmpdir_factory.mktemp("woggle")
    _settings = ConfigFactory(_tmp_dir)("bot.cfg", TEST_CONFIG)
    _bot = BotFactory().preloaded(_settings, ["demo"])
    _bot.enabled_capabilities.add("account-tag")
    return _bot


@pytest.fixture(name="user1")
def fixture_user1(userfactory):
    """
    A user with my_permission permission
    """
    _user = userfactory("user1")
    _user.account = "user1"
    return _user


@pytest.fixture(name="user2")
def fixture_user2(userfactory):
    """
    A user without any permissions
    """
    _user = userfactory("user2")
    _user.account = "user2"
    return _user


def test_demo_account_without_account(bot, user1):
    """
    Tests the .demo-account trigger without an account
    """
    bot.backend.message_sent = []
    bot.on_message(f":{user1.nick} PRIVMSG #test :.demo-account")

    assert (
        len(bot.backend.message_sent) == 0
    ), ".demo-account without account should not output anything"


def test_demo_account_with_account(bot, user1):
    """
    Tests the .demo-account trigger with an account
    """
    bot.backend.message_sent = []
    bot.on_message(
        f"@account={user1.account} " f":{user1.nick} PRIVMSG #test :.demo-account"
    )

    assert (
        len(bot.backend.message_sent) == 1
    ), ".demo-account with account should output exactly one line"
    assert bot.backend.message_sent == rawlist(
        f"PRIVMSG #test :{user1.nick}: Trigger was from the identified user:"
        f" {user1.account}"
    )


def test_demo_permissions_with_permission(bot, user1):
    """
    Tests the .demo_permissions trigger with permissions
    """
    bot.backend.message_sent = []
    bot.on_message(
        f"@account={user1.account} " f":{user1.nick} PRIVMSG #test :.demo-permission"
    )

    assert (
        len(bot.backend.message_sent) == 1
    ), ".demo-permission with permission should output exactly one line"
    assert bot.backend.message_sent == rawlist(
        f"PRIVMSG #test :{user1.nick}: Trigger was from the identified user:"
        f" {user1.account} who has 'my_permission'"
    )


def test_demo_permissions_without_permission(bot, user2):
    """
    Tests the .demo_permissions trigger without permissions
    """

    bot.backend.message_sent = []
    bot.on_message(
        f"@account={user2.account} " f":{user2.nick} PRIVMSG #test :.demo-permission"
    )

    assert (
        len(bot.backend.message_sent) == 0
    ), ".demo-permission without permission should not output anything"
