default: clean install

clean:
	find . -name '*.pyc' -exec rm -rf {} +
	find . -name '__pycache__' -exec rm -rf {} +
	find . -name '*.egg-info' -exec rm -rf {} +

install:
	pip3 install -r requirements.txt
	pip3 install -r dev-requirements.txt

lint:
	pylint --rcfile=.pylintrc plugins tools helpers tests
	black --check --diff plugins tools helpers tests
	isort --check-only --diff plugins tools helpers tests

format:
	black plugins tools helpers tests
	isort plugins tools helpers tests

copyright:
	find . -name *.py -not -size 0 |xargs ./tools/copyright.py

test:
	PYTHONPATH=. coverage run --source helpers,plugins -m pytest
	coverage xml

security:
	bandit helpers plugins

start:
	PYTHONPATH=. sopel start -c woggle-dev.cfg -d

restart:
	PYTHONPATH=. sopel restart -c woggle-dev.cfg

stop:
	PYTHONPATH=. sopel stop -c woggle-dev.cfg

run: 
	PYTHONPATH=. sopel start -c woggle-dev.cfg
